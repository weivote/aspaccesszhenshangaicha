<%
'************************************************************ 
'作者：yujianyue (admin@12391.net) 
'作者主页：http://chalide.cn/
'软件名称：asp+access通用模糊查询系统可增删改查分页 V20221024
'多年前开发的原形，后采用php+mysql技术所以弃用，今整理分享
'使用说明书仅为查询功能的说明书，有asp+access经验用户可以忽略
'适合自己有asp网站环境及access软件并熟练操作的细心用户
'************************************************************

on error resume next

duan=tiaojian1

mdbtype=moexe '只能是xls格式文件哦不要修改

if len(tiaojian1)=2 then
 qianmian1=left(tiaojian1,1)&"&nbsp;&nbsp;"&right(tiaojian1,1)
else
 qianmian1=tiaojian1
end if

Function checkstr(strs)
keyes=split("=|?|%|'|#|*|&|>|<|""","|") '如需修改输入内容修改本行
chakey=""
For m=0 To Ubound(keyes)
rekeys=keyes(m)
if instr(strs,rekeys)>0 then
chakey=chakey&"|"&rekeys
end if
next
checkstr =  chakey
End Function

Function IsFile(FilePath)
 Set Fso=Server.CreateObject("Scri"&"pting.File"&"Sys"&"temObject")
 If (Fso.FileExists(Server.MapPath(FilePath))) Then
 IsFile=True
 Else
 IsFile=False
 End If
 Set Fso=Nothing
End Function

tima = time()

	'==============================
	'函 数 名：FsoFileRead
	'作    用：读取文件
	'参    数：文件相对路径FilePath
	'==============================
	Function FsoFileRead(FilePath,charset)
	Set objAdoStream = Server.CreateObject("A"&"dod"&"b.St"&"r"&"eam")
	objAdoStream.Type=2
	objAdoStream.mode=3  
	objAdoStream.charset=charset
	objAdoStream.open 
	objAdoStream.LoadFromFile Server.MapPath(FilePath) 
	FsoFileRead=objAdoStream.ReadText 
	objAdoStream.Close
	Set objAdoStream=Nothing
	End Function
	

Function filemima(mimafile)
 Set fso = CreateObject("Scripting.FileSystemObject")
 Set fd = fso.OpenTextFile(server.MapPath(mimafile), 1, True)
 if fd.AtEndOfStream=false then
 contentd = fd.readline()
 end if
filemima=trim(contentd)
 fd.close
end Function

Function filekey(texts)
 filekey=0
 rekey="-/-\-%-@-.-"
 keyes=split(rekey,"-")
 nnnnn=Ubound(keyes)
 For m=1 To Ubound(keyes)-1
 rekeys=keyes(m)
 rekeys=trim(rekeys)
 if instr(texts,rekeys)>0 and len(rekeys)>0 then
 filekey=filekey+1
 end if
 next
End Function
Function webtable(AlertStr) 
Response.Write "<table cellspacing=""0""><tbody>"&vbcrlf
response.write "<tr><td colspan=""1"" data-label=""提示"">"&AlertStr&"</td></tr>"
Response.Write "</tbody></table>"&vbcrlf
 Response.End()
End Function
Function AlertUrl(AlertStr,Url) 
 Response.Write "<meta http-equiv=""Content-Type"" content=""text/html; charset=gb2312"" />" &vbcrlf
 Response.Write "<script>" &vbcrlf
 Response.Write "alert('"&AlertStr&"');" &vbcrlf
 Response.Write "location.href='"&Url&"';" &vbcrlf
 Response.Write "</script>" &vbcrlf
 Response.End()
End Function
'==============================
'函 数 名： AlertBack(AlertStr)
'作 用：警告后返回上一页面
'==============================
Function AlertBack(AlertStr) 
 timex = time()
 Response.Write "<meta http-equiv=""Content-Type"" content=""text/html; charset=gb2312"" />" &vbcrlf
 Response.Write "<script>alert('"&AlertStr&"');location.href='?t="&timex&"';</script>"&vbcrlf
 Response.End()
End Function

Function getpage(currentpage,totalpage) 
if currentpage mod 10 = 0 then 
Sp = currentpage \ 10 
else 
Sp = currentpage \ 10 + 1 
end if 
Pagestart = (Sp-1)*10+1 
Pageend = Sp*10 
strSplit = "<a href=""#"" onClick=""chalide(2,1)"" title=""第1页"">第1页</a>" &vbcrlf
if Sp > 1 then strSplit = strSplit & "<a href=""#"" onClick=""chalide(2,"&Pagestart-10&")"">...</a>" &vbcrlf
for j=PageStart to Pageend 
if j > totalpage then exit for 
if j&"@" <> currentpage&"@" then 
strSplit = strSplit & "<a href=""#"" onClick=""chalide(2,"&j&")"">第"&j&"页</a>" &vbcrlf
else 
strSplit = strSplit & "第<font color=""red"" id=""npage"">"&j&"</font>页" &vbcrlf
end if 
next 
if Sp*10 < totalpage then strSplit = strSplit & "<a href=""#"" onClick=""chalide(2,"&Pagestart+10&")"">...</a>" &vbcrlf
strSplit = strSplit & "<a href=""#"" onClick=""chalide(2,"&totalpage&")"">第"&totalpage&"页</a>" &vbcrlf
getpage = strSplit 
End Function
%>