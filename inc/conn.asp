<% 
Response.Charset = "GB2312"
'************************************************************ 
'作者：yujianyue (admin@12391.net) 
'作者主页：http://chalide.cn/
'软件名称：asp+access通用模糊查询系统可增删改查分页 V20221024
'多年前开发的原形，后采用php+mysql技术所以弃用，今整理分享
'使用说明书仅为查询功能的说明书，有asp+access经验用户可以忽略
'适合自己有asp网站环境及access软件并熟练操作的细心用户
'************************************************************

title="asp+access通用模糊查询系统可增删改查分页"	'#设置查询标题,相信你懂的。

copysr="查立得"					'#设置底部版权文字,相信你懂的。
copysu="https://chalide.com/"			'#设置底部版权连接,相信你懂的。

'数据库相关
UpDir="shujukufangzheli" 			'修改为只有你自己知道的内容,并修改对应文件夹名称
times ="20180608181818"				'#access数据文件名，不必后缀。
moexe =".asa"					'建议后缀 .mdb修改为.asa 并与实际相符
biaogege1="chengji"				'跟数据表名称一致, 默认查询第一个表
tiaojian1="姓名|学号"				'查询条件字段 多个条件|隔开

hidelie = "|ID|id|"				'隐藏列，规律: |号开始结尾隔开
editlie = "|科目1|科目2|科目3|"			'可改列，规律: |号开始结尾隔开

descfile="../inc/desc.txt"			'文字说明对应文件路径
timex ="2021@1024@12391"			'修改js css修改本参数可以去浏览器缓存。

'注意：ajax查后才有增删改，本页设置字段才能改，多页内容才有分页
'注意事项：保留ID字段；其他字段保持文本字段
%>
